# README #

This project is a Liferay 6.2 hook

### What is this repository for? ###

This hook enables the Liferay APIs to accept body input

## Requirements  
| Framework                                                                        |Version  |License             |Note 																	|
|--------------------------------------------------------------------------------------------|---------|--------------------|-------------------------------------------------------------------------|
|[Java SE Development Kit](https://www.java.com/it/download/help/index_installing.xml?j=7) |7.0 |[Oracle Binary Code License](http://www.oracle.com/technetwork/java/javase/terms/license/index.html)         |Also v.8|
|[Liferay Portal CE bundled with Tomcat](https://sourceforge.net/projects/lportal/files/Liferay%20Portal/6.2.3%20GA4/liferay-portal-tomcat-6.2-ce-ga4-20150416163831865.zip/download) |6.2 GA4 | [GNU LGPL](https://www.liferay.com/it/downloads/ce-license)      ||
|[postgreSQL](https://www.postgresql.org/download/)| 9.3  |[PostgreSQL License](https://www.postgresql.org/about/licence/) ||
|[Social Office CE](https://web.liferay.com/it/community/liferay-projects/liferay-social-office/overview) | 3.0 | GNU LGPL |

## Libraries

CDV is based on the following software libraries and frameworks.

|Framework                           |Version       |Licence                                      |
|------------------------------------|--------------|---------------------------------------------|
|[Apache Axis](http://axis.apache.org/axis/) | |[Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0) |
|Apache commons discovery | 0.2|Apache Software License, Version 1.1 |
|[Java Architecture For XML Binding](https://github.com/javaee/jax-rpc-ri) | 1.1|CDDL Version 1.1 |
|[JSAAJ API](http://java.sun.com/webservices/saaj/index.jsp) | 1.3|CDDL Version 1.0 |
|[WSDL4J](http://sourceforge.net/projects/wsdl4j) | 1.6.2|[Common Public License Version 1.0](https://opensource.org/licenses/cpl1.0.php) |

### How do I get set up? ###

For details about configuration/installation you can see "D2.4 – WELIVE OPEN INNOVATION AND CROWDSOURCING TOOLS V2"

### Who do I talk to? ###

ENG team, Antonino Sirchia, Filippo Giuffrida

### Copying and License

This code is licensed under [Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0)