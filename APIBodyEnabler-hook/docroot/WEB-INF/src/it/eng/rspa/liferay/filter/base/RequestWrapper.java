package it.eng.rspa.liferay.filter.base;

import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

public class RequestWrapper extends HttpServletRequestWrapper {
	private Map<String, String[]> paramsMap = null;
	
	public RequestWrapper(ServletRequest request, Map<String,String> furtherParameters) {
		super((HttpServletRequest)request);
		this.paramsMap = new HashMap<String, String[]>();
		this.paramsMap.putAll(super.getParameterMap());
		
		for(String key:furtherParameters.keySet()){
			String value = furtherParameters.get(key);
			this.paramsMap.put(key, new String[]{value});
		}
	}
	
	@Override
	public Map<String, String[]> getParameterMap() {
		return Collections.unmodifiableMap(this.paramsMap);
	}

	@Override
    public Enumeration<String> getParameterNames() {
        return Collections.enumeration(getParameterMap().keySet());
    }

    @Override
    public String[] getParameterValues(final String name) {
        return getParameterMap().get(name);
    }
}
