package it.eng.rspa.liferay.filter.base;

import java.io.BufferedReader;
import java.io.IOException;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.servlet.HttpMethods;

public class BodyEnabler implements Filter{

	private Log logger;
	private String moduleName;
	
	@Override
	public void destroy() {
		logger.info("Destroying "+moduleName);
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) 
			throws IOException, ServletException {
		
		HttpServletRequest httpRequest = (HttpServletRequest) request;
		ServletRequest chainableRequest = httpRequest;
		
		Map<String, String> moreParameters = new HashMap<String,String>();
		
		/* Retrieving the query parameters */
		String queryString = httpRequest.getQueryString();
		if(queryString!=null){
			if(queryString.startsWith("?")){
				queryString = queryString.substring(1);
			}

			String[] couples = queryString.split("&");
			for(String couple : couples){
				String[] param = couple.split("=");
				moreParameters.put(URLDecoder.decode(param[0], "UTF-8"), URLDecoder.decode(param[1], "UTF-8"));
			}
		}
		
		/* Retrieving the body */
		if(!httpRequest.getMethod().equalsIgnoreCase(HttpMethods.GET)){
			String body = convertToString(request.getReader());
			
			moreParameters.put("body", body);
			chainableRequest = (ServletRequest)(new RequestWrapper(httpRequest, moreParameters));
		}

		/* Pass request back down the filter chain */
		chain.doFilter(chainableRequest, response);
	}

	@Override
	public void init(FilterConfig config) throws ServletException {
		moduleName = config.getInitParameter("custom-name");
		logger = LogFactoryUtil.getLog(moduleName.replaceAll(" ", "-"));
		
		logger.info(moduleName+" initialized");
	}
	
	private String convertToString(BufferedReader br) throws IOException{
		StringBuilder sb = new StringBuilder();
        String line;
        while ((line = br.readLine()) != null) {
            sb.append(line+"\n");
        }
        br.close();
        return sb.toString();
	}

}
